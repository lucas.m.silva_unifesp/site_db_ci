from flask import Flask, jsonify

app = Flask(__name__)

# Sample data
members = [
    {"id": 1, "name": "John Doe", "age": 25},
    {"id": 2, "name": "Jane Smith", "age": 30},
    {"id": 3, "name": "Bob Johnson", "age": 35}
]

@app.route('/members', methods=['GET'])
def get_members():
    return jsonify(members)

@app.route('/members/<int:id>', methods=['GET'])
def get_member(id):
    member = next((member for member in members if member["id"] == id), None)
    if member:
        return jsonify(member)
    else:
        return jsonify({"error": "Member not found"}), 404

if __name__ == '__main__':
    app.run()