import React, {useState, useEffect} from 'react';

function App() {

  const [data, setData] = useState([{}]);

  useEffect(() => {
    fetch("/members").then(
      res => res.json()
    ).then(
      data => {
        setData(data)
        console.log(data)
    })
  })

  return ( 
    <div style={styles.wrapper}>
      {(typeof data.map(member => member) === "undefined") ? (
        <p>Loading...</p>
      ) : (
        data.map((member,i) => (
          <p key={i}>i: {i} Name: {member.name}, id: {member.id}</p>
        ))
      )}
    </div>
  );
}
 
export default App;

const styles = {
  wrapper: {
    textAlign: "center",
    maxWidth: "950px",
    margin: "0 auto",
    border: "1px solid #e6e6e6",
    padding: "40px 25px",
    marginTop: "50px", 
    color: "#790578"
  }
};